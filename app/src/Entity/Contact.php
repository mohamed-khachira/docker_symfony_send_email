<?php

namespace App\Entity;

use Symfony\Component\Validator\Constraints as Assert;

class Contact
{
    /**
     * @Assert\NotBlank
     * @Assert\Length(min=2, max=100)
     */
    protected ?string $firstName;

    /**
     * @Assert\NotBlank
     * @Assert\Length(min=2, max=100)
     */
    protected ?string $lastName;

    /**
     * @Assert\NotBlank
     * @Assert\Email(
     *     message = "The email '{{ value }}' is not a valid email."
     * )
     */
    protected ?string $email;

    /**
     * @Assert\NotBlank
     * @Assert\Length(min=10)
     */
    protected ?string $message;

    /**
     * Get the value of firstName
     */
    public function getFirstName(): ?string
    {
        return $this->firstName;
    }

    /**
     * Set the value of firstName
     *
     * @return  self
     */
    public function setFirstName(?string $firstName): self
    {
        $this->firstName = $firstName;

        return $this;
    }

    /**
     * Get the value of lastName
     */
    public function getLastName(): ?string
    {
        return $this->lastName;
    }

    /**
     * Set the value of lastName
     *
     * @return  self
     */
    public function setLastName(?string $lastName): self
    {
        $this->lastName = $lastName;

        return $this;
    }

    /**
     * Get message = "The email '{{ value }}' is not a valid email."
     */
    public function getEmail(): ?string
    {
        return $this->email;
    }

    /**
     * Set message = "The email '{{ value }}' is not a valid email."
     *
     * @return  self
     */
    public function setEmail(?string $email): self
    {
        $this->email = $email;

        return $this;
    }

    /**
     * Get the value of message
     */
    public function getMessage(): ?string
    {
        return $this->message;
    }

    /**
     * Set the value of message
     *
     * @return  self
     */
    public function setMessage(?string $message): self
    {
        $this->message = $message;

        return $this;
    }
}
