<?php

namespace App\Service;

use App\Entity\Contact;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Bridge\Twig\Mime\TemplatedEmail;

class Mailer
{
    private $mailer;
    public function __construct(MailerInterface $mailer)
    {
        $this->mailer = $mailer;
    }

    public function sendRequestMessage(Contact $contact)
    {
        $email = (new TemplatedEmail())
            ->from($contact->getEmail())
            ->to('request@mail.com')
            ->subject("Request [{$contact->getFirstName()} {$contact->getLastName()}]")
            ->htmlTemplate('email/request.html.twig')
            ->context([
                'message' => $contact->getMessage(),
            ]);

        $this->mailer->send($email);
    }
}
