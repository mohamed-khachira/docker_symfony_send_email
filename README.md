# symfony5.2-php7.4

```
git clone https://gitlab.com/mohamed-khachira/docker_symfony_send_email.git

cd docker_symfony_send_email

make build-run

| App url                | Mailhog url               | Adminer url              |
| ---------------------- | ------------------------- | -------------------------|
| http://localhost:8888/ | http://localhost:8025     | http://localhost:8085    |

```
